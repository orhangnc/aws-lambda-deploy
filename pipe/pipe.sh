#!/bin/bash
#
# Update an AWS Lambda function with new code, publishing a new version that points to the updated fuction.
# See https://docs.aws.amazon.com/cli/latest/reference/lambda/update-function-code.html
#
# Required globals:
#   AWS_ACCESS_KEY_ID
#   AWS_SECRET_ACCESS_KEY
#   AWS_DEFAULT_REGION
#   COMMAND - alias | update
#   FUNCTION_NAME   - update
#   ALIAS     - alias
#   ZIP_FILE  - update
#   FUNCTION_VERSION  - alias
#
# Optional globals:
#   DEBUG (true or false)

SCRIPT_PATH=$(dirname "$0")
source "${SCRIPT_PATH}"/common.sh
source "${SCRIPT_PATH}"/update-lambda.sh
source "${SCRIPT_PATH}"/alias-lambda.sh
source "${SCRIPT_PATH}"/update-lambda-configuration.sh

parse_environment_variables() {
  AWS_ACCESS_KEY_ID=${AWS_ACCESS_KEY_ID:?'AWS_ACCESS_KEY_ID variable missing.'}
  AWS_SECRET_ACCESS_KEY=${AWS_SECRET_ACCESS_KEY:?'AWS_SECRET_ACCESS_KEY variable missing.'}
  AWS_DEFAULT_REGION=${AWS_DEFAULT_REGION:?'AWS_DEFAULT_REGION variable missing.'}
  COMMAND=${COMMAND:?'COMMAND variable missing.'}
  FUNCTION_NAME=${FUNCTION_NAME:?'FUNCTION_NAME variable missing.'}

  if [[ "${COMMAND}" == "update" ]]; then
      ZIP_FILE=${ZIP_FILE:?'ZIP_FILE variable missing.'}
  elif [[ "${COMMAND}" == "alias" ]]; then
      filename=$BITBUCKET_PIPE_SHARED_STORAGE_DIR/aws-lambda-deploy-env
      if [ -z "${VERSION}" ] && [ -f $filename ]; then
        # try to fetch from shared data storage
        VERSION=$(jq  --raw-output '.Version' $filename)
      fi
      ALIAS=${ALIAS:?'ALIAS variable missing.'}
      VERSION=${VERSION:?'VERSION variable missing.'}
  else
      error "Unknown command."
  fi
}

enable_debug
parse_environment_variables
if [[ "${COMMAND}" == "alias" ]]; then
  alias
else
  update
fi

if [[ ! -z "${FUNCTION_CONFIGURATION}" ]]; then
  update_lambda_configuration
fi